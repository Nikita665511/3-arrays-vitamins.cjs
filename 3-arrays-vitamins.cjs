const items = [{
    name: 'Orange',
    available: true,
    contains: "Vitamin C",
}, {
    name: 'Mango',
    available: true,
    contains: "Vitamin K, Vitamin C",
}, {
    name: 'Pineapple',
    available: true,
    contains: "Vitamin A",
}, {
    name: 'Raspberry',
    available: false,
    contains: "Vitamin B, Vitamin A",

}, {
    name: 'Grapes',
    contains: "Vitamin D",
    available: false,
}];


function availableitems(items) {
    const result = items.filter((items) => items.available );
    return result;
}
//console.log(availableitems(items));

function itemscontainingvitaminc(items){
    let anwser = items.find(item => item.contains === "Vitamin C");
    return anwser;
}
//console.log(itemscontainingvitaminc(items));

function itemscontainingvitaminA(items) {
   let anwser = items.filter((item) => item.contains.includes("Vitamin A"));
       return anwser;
    }  
    
//console.log(itemscontainingvitaminA(items));

function groupitemsbyvitamin(items) {
   let anwser = items.reduce((acc, item)=>{
    let arr = item.contains.split(', ');
    arr.map((elements)=>{
        if(acc.hasOwnProperty(elements)){
           acc[elements].push(item.name);
        }
        else{
            acc[elements] = [];
           acc[elements].push(item.name);
        }

    })
    return acc;
},{});
 return anwser;
}
//console.log(groupitemsbyvitamin(items));

function sortofvitamins(items){
    return items.sort(function(A,B){
        let Avitamin = A.contains.split(', ');
    let Bvitamin = B.contains.split(', ');
        if(Avitamin.length>Bvitamin.length)
            return +1;
        else if(Avitamin.length<Bvitamin.length)
            return -1;
        else
            return 0;
    })
}
//console.log(sortofvitamins(items));

